﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Constants.Controllers
{
    public class PartnerControllerConstants
    {
        public const string constErrorMessagePartnerIsNotActive = "Данный партнер не активен";
        public const string constErrorMessageLimitShouldBePisitive = "Лимит должен быть больше 0";
    }
}
