﻿using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Constants.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _partnerRepositoryMock = new Mock<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepositoryMock.Object);
        }

        private Partner CreateTestPartner()
        {
            Partner partner = new Partner()
            {
                Id = Guid.NewGuid(),
                IsActive = true,
                Name = "Test Partner",
                NumberIssuedPromoCodes = 0,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = new DateTime(),
                        Limit =10
                    }
                }
            };

            return partner;
        }

        private SetPartnerPromoCodeLimitRequest CreateTestSetPartnerPromoCodeLimitRequest()
        {
            SetPartnerPromoCodeLimitRequest request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 20,
                EndDate = new DateTime().AddDays(10)
            };

            return request;
        }


        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldReturn404_IfPartnerIsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var notFoundResult = result as NotFoundResult;

            // Assert
            notFoundResult.Should().NotBe(null);
            notFoundResult.StatusCode.Should().Equals(404);
        }


        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldReturn400_IfPartnerIsNotActive()
        {
            // Arrange
            var partner = CreateTestPartner();
            partner.IsActive = false;
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var badResult = result as BadRequestObjectResult;

            // Assert
            badResult.Should().NotBe(null);
            badResult.StatusCode.Should().Equals(400);
            badResult.Value.ToString().Should().Equals(PartnerControllerConstants.constErrorMessagePartnerIsNotActive);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldSetZeroPromocodes_IfSetLimit()
        {
            // Arrange
            var partner = CreateTestPartner();
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var createdResult = result as CreatedAtActionResult;
            

            // Assert
            createdResult.Should().NotBe(null);
            createdResult.StatusCode.Should().Equals(201);
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то если лимит закончился, то количество промокодов не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldPromoCodCountNotBeChanged_IfAllLimitIsFinish()
        {
            // Arrange
            var shouldBeValue = 10;
            var partner = CreateTestPartner();
            partner.NumberIssuedPromoCodes = shouldBeValue;
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Заканчиваем все лимиты партнера
            foreach (var partnerPromoCodeLimit in partner.PartnerLimits)
            {
                partnerPromoCodeLimit.CancelDate = DateTime.Now.AddDays(10);
            }
            

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var createdResult = result as CreatedAtActionResult;


            // Assert
            createdResult.Should().NotBe(null);
            createdResult.StatusCode.Should().Equals(201);
            partner.NumberIssuedPromoCodes.Should().Be(shouldBeValue);
        }


        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldCancelPriorLimit_IfSetNewLimit()
        {
            // Arrange
            var partner = CreateTestPartner();
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var shouldBeCanceledLimitGuid = partner.PartnerLimits.FirstOrDefault(x => x.CancelDate == null).Id;
            


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var createdResult = result as CreatedAtActionResult;


            // Assert
            createdResult.Should().NotBe(null);
            createdResult.StatusCode.Should().Equals(201);
            partner.PartnerLimits.FirstOrDefault(x=>x.Id == shouldBeCanceledLimitGuid).CancelDate.Should().NotBeNull();
        }


        /// <summary>
        /// Лимит должен быть больше 0;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ShouldBeBadRequest_IfRequestLimitValueIsNegative()
        {
            // Arrange
            var partner = CreateTestPartner();
            var request = CreateTestSetPartnerPromoCodeLimitRequest();
            _partnerRepositoryMock.Setup(x => x.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);
            
            // устанавливаемый лимит < 0
            request.Limit = -1;


            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var badResult = result as BadRequestObjectResult;


            // Assert
            badResult.Should().NotBe(null);
            badResult.StatusCode.Should().Equals(400);
            badResult.Value.ToString().Should().Equals(PartnerControllerConstants.constErrorMessageLimitShouldBePisitive);
        }
    }
}