﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.UnitTests.TestObjectBuilders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncInMemoryDbTests : IClassFixture<TestFixtureInMemory>
    {
        private readonly IRepository<Partner> _partnerRepository;
        private readonly PartnersController _partnersController;


        public SetPartnerPromoCodeLimitAsyncInMemoryDbTests(TestFixtureInMemory testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
            _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnerRepository);
        }


        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitShouldBeSavedinDB()
        {
            // Arrange
            var testDate = new DateTime();
            var testLimit = 100;

            var requestBuilder = new SetPartnerPromoCodeLimitRequestBuilder();
            requestBuilder.WithCreatedLimit(testLimit);
            requestBuilder.WithCreatedEndDate(testDate);
            var request = requestBuilder.Build();
            

           
            // Act
            var setLimitResult = await _partnersController.SetPartnerPromoCodeLimitAsync(DbSeed.SeedPartnerGuid, request);
            var createdLimitResult = setLimitResult as CreatedAtActionResult;
            var UpdatedPartner = await _partnerRepository.GetByIdAsync(DbSeed.SeedPartnerGuid);


            // Assert
            createdLimitResult.Should().NotBe(null);
            createdLimitResult.StatusCode.Should().Equals(201);
            UpdatedPartner.PartnerLimits.FirstOrDefault(x => x.Limit == testLimit && x.EndDate == testDate).Should().NotBeNull();
        }
    }
}
