﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using  Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class DbSeed
    {
        public static readonly Guid SeedPartnerGuid = Guid.NewGuid();

        public static void Seed(IServiceProvider serviceProvider)
        {
            var dbContext = serviceProvider.GetService<DataContext>();
            SeedPartners(dbContext);

        }

        public static void SeedPartners(DataContext dbContext)
        {
            Partner partner = new Partner()
            {
                Id = SeedPartnerGuid,
                IsActive = true,
                Name = "Test Partner",
                NumberIssuedPromoCodes = 5,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = new DateTime(),
                        Limit = 10
                    }
                }
            };

            dbContext.Partners.Add(partner);
            dbContext.SaveChanges();
        }
    }
}
