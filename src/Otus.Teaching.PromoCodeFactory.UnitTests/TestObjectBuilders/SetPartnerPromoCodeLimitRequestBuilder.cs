﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.TestObjectBuilders
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        private int _limit;
        private DateTime _endDate;

        public SetPartnerPromoCodeLimitRequestBuilder WithCreatedLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder WithCreatedEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = _limit,
                EndDate = _endDate
            };
        }
    }
}
