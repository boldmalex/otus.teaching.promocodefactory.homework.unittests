﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class TestFixtureInMemory
    {
        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }

        public TestFixtureInMemory()
        {
            ServiceCollection = new ServiceCollection();// Configuration.GetServiceCollection(configuration, "Tests");
            var serviceProvider = GetServiceProvider();
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            DbSeed.Seed(serviceProvider);
            return serviceProvider;
        }
    }
}
